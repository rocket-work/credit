# api.credit.rocketwork

## Getting started
- Use specified at `.env.docker-compose` variables or set custom port mappings and services env variables at will.
- App shipped with a Docker definition that makes it easy to get a containerized development environment up and running. If you do not already have Docker on your computer, it's the right time to install it
- Open a terminal, and navigate to the directory containing this file. Run the following command to start all services using Docker Compose:
```shell
# to download, build, start in detached mode the latest versions of the images:
make up 

# to download and build the latest versions of the images:
make build

# to start in attached:
make start

# to stop:
make down

# enter in php container:
make php

# list of other commands:
make help
```

## Api specification:
Api specification generated after app started & running and available at: 

---
NOTE:
[api.credit.rocketwork.localhost:4260](api.credit.rocketwork.localhost:4260) specified as value of *SERVER_NAME* in `.env.docker-compose`
---

- [swagger-ui](https://api.credit.rocketwork.localhost:4260/) 
- [Re Roc](https://api.credit.rocketwork.localhost:4260/docs?ui=re_doc) 

## Test runnig
- `php vendor/bin/phpunit` or `composer test` - run all tests in *php container* (make php)

You aslo can use the short alias with custom wrapped shortcuts for `php vendor/bin/phpunit` wish is `t` and placed in `bin/t` - to run all tests.

List of available shorcuts of `t`:
- `t [pathOfDirOrFile]` - Run all tests in specified by [pathOfDirOrFile] path include sub directories or all tests in specified file
- `t [pathOfDirOrFile][:filter]` - Run tests whose method name contains the [filter] value in path specified by [pathOfDirOrFile]
- `t [-i]` - Output more debug information

## Code style checker, static code analysis, tests
- in *php container* (make php)
```shell
# check code style & static code analysis, tests
check-all

# run static code analysis
check-psalm


# automatically fix code style if possible
fix-cs 

# check code style
composer cs 

```
- in *php container* (make php)
```shell
# check code style & run static code analysis
make check

# or inside *php container* (make php)
check-all
check-psalm

# automatically fix code style if possible
composer cs-fix 

# check code style
composer cs 

# run static code analysis
composer static-analysis
```
