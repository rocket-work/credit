<?php

namespace App\Tests\Api\Lead;

use App\Entity\Lead;
use App\Factory\LeadFactory;
use App\Tests\Core\AppApiTestCase;

class PostLeadTest extends AppApiTestCase
{
    public function testOk(): void
    {
        // act
        self::sendJsonRequest(
            method: 'POST',
            url: 'v1/leads',
            json: $input = [
                'phone' => $this->faker()->e164PhoneNumber,
            ],
        );

        // assert
        self::assertResponseStatusCodeSame(201);

        /** @var Lead|null $lead */
        $lead = LeadFactory::repository()->last();

        self::assertNotNull($lead);
        $this->assertSame(expected: $input['phone'], actual: $lead->getPhone());
    }

    public function testWrongPhoneFormat(): void
    {
        // act
        self::sendJsonRequest(
            method: 'POST',
            url: 'v1/leads',
            json: $input = [
                'phone' => $this->faker()->bothify('??'),
            ],
        );

        // assert
        self::assertResponseStatusCodeSame(422);
    }

    public function testAllData(): void
    {
        // act
        self::sendJsonRequest(
            method: 'POST',
            url: 'v1/leads',
            json: $input = [
                'phone' => $this->faker()->e164PhoneNumber,
                'name' => $this->faker()->words(3, asText: true),
            ],
        );

        // assert
        self::assertResponseStatusCodeSame(201);

        /** @var Lead|null $lead */
        $lead = LeadFactory::repository()->last();

        self::assertNotNull($lead);
        $this->assertSame(expected: $input['phone'], actual: $lead->getPhone());
        $this->assertSame(expected: $input['name'], actual: $lead->getName());
    }
}
