<?php

namespace App\Tests\Api\Lead;

use App\Entity\Lead;
use App\Factory\LeadFactory;
use App\Tests\Core\AppApiTestCase;

class PatchLeadTest extends AppApiTestCase
{
    public function testOk(): void
    {
        // arrange
        $lead = LeadFactory::createOne();

        // act
        self::sendJsonRequest(
            method: 'PATCH',
            url: sprintf('v1/leads/%s', $lead->getId()),
            json: $input = [
                'name' => $this->faker()->words(3, asText: true),
            ],
        );

        // assert
        self::assertResponseStatusCodeSame(200);

        $this->assertSame(expected: $input['name'], actual: $lead->getName());
    }
}
