<?php

namespace App\Tests\Core;

use App\Tests\Core\Mixins\AppTestCaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

abstract class AppTestCase extends KernelTestCase
{
    use AppTestCaseTrait;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel(['environment' => 'test', 'debug' => true]);
        $this->setServicesFromContainer();
        $this->runBeforeEachTest();
    }
}
