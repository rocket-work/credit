<?php

/** @noinspection PhpMultipleClassDeclarationsInspection */

namespace App\Tests\Core\Mixins;

use DAMA\DoctrineTestBundle\Doctrine\DBAL\StaticDriver;
use Doctrine\ORM\EntityManager;
use Faker\Factory;
use Faker\Generator;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\VarDumper\VarDumper;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\Test\Factories;

trait AppTestCaseTrait
{
    use Factories;

    public function getProxyObject(mixed $data): object
    {
        if ($data instanceof Proxy) {
            return $data->object();
        }

        return $data;
    }

    public function getFromContainer(string $id): mixed
    {
        return static::getContainer()->get($id);
    }

    protected function getParameter(string $name, mixed $default = null): mixed
    {
        return $this->getFromContainer('parameter_bag')->get($name, $default);
    }

    protected function runBeforeEachTest(): void
    {
        return;
    }

    protected function faker(): Generator
    {
        static $faker = null;

        if ($faker === null) {
            $faker = Factory::create();
        }

        return $faker;
    }

    protected function debug(...$vars): void
    {
        StaticDriver::commit();

        foreach ($vars as $v) {
            VarDumper::dump($v);
        }

        exit(1);
    }

    protected function setServicesFromContainer(): void
    {
        $class             = new ReflectionClass($this);
        $properties        = $class->getProperties(ReflectionProperty::IS_PUBLIC);

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            $propertyType = (string)$property->getType();

            if (static::getContainer()->has($propertyType)) {
                $this->{$propertyName} = static::getContainer()->get($propertyType);
            }
        }
    }

    protected function entityManager(): EntityManager
    {
        return $this->getFromContainer('doctrine')->getManager();
    }
}
