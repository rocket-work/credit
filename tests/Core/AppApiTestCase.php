<?php

namespace App\Tests\Core;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Tests\Core\Mixins\AppTestCaseTrait;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @psalm-suppress PropertyNotSetInConstructor
 */
abstract class AppApiTestCase extends ApiTestCase
{
    use AppTestCaseTrait;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel(['environment' => 'test', 'debug' => false]);
        $this->setServicesFromContainer();
        $this->runBeforeEachTest();
    }

    public static function sendJsonRequest(
        string $method,
        string $url,
        array $json = [],
    ): ResponseInterface {
        $headers = [
            'Content-Type' => ['application/json'],
            'accept' => ['application/json'],
        ];

        $client = self::createClient(
            kernelOptions: [],
            defaultOptions: ['headers' => $headers]
        );

        return $client->request(
            method: $method,
            url: $url,
            options: [
                'json' => $json,
            ]
        );
    }
}
