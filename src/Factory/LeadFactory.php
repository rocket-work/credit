<?php

namespace App\Factory;

use App\Entity\Lead;
use App\Repository\LeadRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Lead>
 *
 * @method static Lead|Proxy createOne(array $attributes = [])
 * @method static Lead[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Lead[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Lead|Proxy find(object|array|mixed $criteria)
 * @method static Lead|Proxy findOrCreate(array $attributes)
 * @method static Lead|Proxy first(string $sortedField = 'id')
 * @method static Lead|Proxy last(string $sortedField = 'id')
 * @method static Lead|Proxy random(array $attributes = [])
 * @method static Lead|Proxy randomOrCreate(array $attributes = [])
 * @method static Lead[]|Proxy[] all()
 * @method static Lead[]|Proxy[] findBy(array $attributes)
 * @method static Lead[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Lead[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static LeadRepository|RepositoryProxy repository()
 * @method Lead|Proxy create(array|callable $attributes = [])
 */
final class LeadFactory extends ModelFactory
{
    /**
     * @psalm-suppress UndefinedMagicMethod
     */
    protected function getDefaults(): array
    {
        return [
            'phone' => self::faker()->e164PhoneNumber,
            'name' => self::faker()->words(2, asText: true),
        ];
    }

    protected static function getClass(): string
    {
        return Lead::class;
    }
}
