<?php

namespace App\Repository;

use App\Entity\Lead;

/**
 * @extends AbstractRepository<Lead>
 *
 * @method Lead      get($id)
 * @method Lead|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lead|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lead[]    findAll()
 * @method Lead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadRepository extends AbstractRepository
{
    public string $entityClass = Lead::class;
}
