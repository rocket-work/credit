<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use InvalidArgumentException;
use RuntimeException;

/**
 * @template T of object
 * @template-extends ServiceEntityRepository<T>
 */
class AbstractRepository extends ServiceEntityRepository
{
    public string $entityClass = '';

    public function __construct(ManagerRegistry $registry)
    {
        /** @psalm-suppress  ArgumentTypeCoercion */
        parent::__construct($registry, $this->entityClass);
    }

    public function get(string $id): object
    {
        if (!$object = $this->find($id)) {
            throw new InvalidArgumentException(sprintf('%s with id "%s" is not found', $this->entityClass, $id));
        }

        return $object;
    }

    public function save(object $entity, bool $flush = true): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(object $entity, bool $flush = true): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
