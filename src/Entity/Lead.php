<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Repository\LeadRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Ulid;
use Symfony\Component\Validator\Constraints as Assert;
use Misd\PhoneNumberBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;

#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(
            security: true,
        ),
        new Patch(
            denormalizationContext: ['groups' => ['lead:update']],
            security: true,
        )
    ],
    routePrefix: 'v1',
    normalizationContext: ['groups' => ['lead:read']],
    denormalizationContext: ['groups' => ['lead:write']]
)]
#[ORM\Entity(repositoryClass: LeadRepository::class)]
#[ORM\Table()]
class Lead
{
    #[ApiProperty(example: '01GFKSWG6XX71Z25D3ZCNW7N13')]
    #[Assert\Ulid]
    #[Groups(['lead:read'])]
    #[ORM\Id]
    #[ORM\Column(length: 26, unique: true)]
    private string $id;

    #[ApiProperty(example: '+7 900 123 12 12')]
    #[Assert\NotBlank]
    #[AssertPhoneNumber(type: [AssertPhoneNumber::MOBILE])]
    #[Groups(['lead:write'])]
    #[ORM\Column(length: 60)]
    private string $phone = '';

    #[ApiProperty(example: 'Hermann Freeman')]
    #[Assert\Length(max: 60)]
    #[Groups(['lead:write', 'lead:update'])]
    #[ORM\Column(length: 60, nullable: true)]
    private ?string $name = null;

    public function __construct()
    {
        $this->id = Ulid::generate();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}
