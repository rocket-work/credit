# Executables (local)
DOCKER_COMP = docker-compose --env-file .env.docker-compose

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec php

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer
SYMFONY  = $(PHP_CONT) bin/console

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up run down logs sh composer vendor sf cc

##—————— api.credit.rocketwork ——————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s \033[0m %s\n", $$1 e, $$2}' |  sed -e 's/\[32m##/[33m/'

##—————— ✔ Checkers ——————
check-all: ## Check code style, run static code analysis & app tests
	$(COMPOSER) check-all

##—————— 🐳 Docker ——————
build: ## Builds the Docker images
	$(DOCKER_COMP) build --pull --no-cache

rebuild: ## Rebuilds the Docker images (with cache)
	$(DOCKER_COMP) build

chown: ## Change the owner of file system files and directories
	$(DOCKER_COMP) run --rm php chown -R 1000:1000 .

ps: ## List containers
	$(DOCKER_COMP) ps

run: ## Start the docker hub in attached mode (with logs)
	$(DOCKER_COMP) up

up: ## Start the docker hub in detached mode (no logs)
	$(DOCKER_COMP) up --detach

logs: ## Show live logs
	$(DOCKER_COMP) logs --tail=0 --follow

php: ## Connect to the PHP FPM container
	$(PHP_CONT) sh

down: ## Stop the docker hub
	$(DOCKER_COMP) down --remove-orphans

##—————— 🧙 Composer ——————
composer: ## Run composer, pass the parameter "c=" to run a given command, example: make composer c='req symfony/orm-pack'
	$(eval c ?=)
	$(COMPOSER) $(c)

vendor: ## Install vendors according to the current composer.lock file
vendor:
	$(COMPOSER) install --prefer-dist --no-progress --no-scripts --no-interaction

##—————— 🎵 Symfony ——————
s: ## List all Symfony commands or pass the parameter "c=" to run a given command, example: make s c=about
	$(eval c ?=)
	$(SYMFONY) $(c)

cc: c=c:c ## Clear the cache
cc: s

##—————— ✔ Checkers ——————
check: ## Run: code style checker, static code analysis, app tests
	c=check
